from bokeh.plotting import figure, output_file, show, save
from bokeh.layouts import row, column
from bokeh.embed import file_html, components
from bokeh.resources import INLINE
from bokeh.models import CustomJS, ColumnDataSource, DataTable,TableColumn,ColumnDataSource,Button
from random import random
from os.path import join,dirname

import flask
from flask_cors import CORS

app = flask.Flask(__name__)
CORS(app)

@app.route("/")
def index():

    ids = [x for x in range(500)]
    x = [random() for x in range(500)]
    y = [random() for y in range(500)]
    data = dict(id=ids,x=x,y=y)
    columns=[TableColumn(field="id", title="ids"),
        TableColumn(field="x", title="X"),
        TableColumn(field="y", title="Y"),]

    source=ColumnDataSource(data)
    table = DataTable(
        source=source,
        columns=columns,
        width=800,
        editable=True,
    )

    button = Button(label="Download", button_type="success")
    button.callback = CustomJS(args=dict(source=source),
            code=open(join(dirname(__file__), "download.js")).read())

    s1 = ColumnDataSource(data=dict(x=x, y=y))
    p1 = figure(plot_width=400, plot_height=400, tools="pan, lasso_select, box_select, box_zoom, reset, save", title="Lo' dato' señore'")
    p1.circle('x', 'y', source=s1, alpha=0.6)

    s2 = ColumnDataSource(data=dict(x=[], y=[]))
    p2 = figure(plot_width=400, plot_height=400, x_range=(0, 1), y_range=(0, 1),
                tools="", title="Watch Here")
    p2.circle('x', 'y', source=s2, alpha=0.6)

    s1.callback = CustomJS(args=dict(table=table,source=source), code="""
            var inds = cb_obj.selected['1d'].indices;
            var d1 = cb_obj.data;
            var d2 = source.data;
            d2['id'] = []
            d2['x'] = []
            d2['y'] = []
            for (i = 0; i < inds.length; i++) {
                d2['id'].push(inds[i])
                d2['x'].push(d1['x'][inds[i]])
                d2['y'].push(d1['y'][inds[i]])
            }
            source.change.emit();
            table.change.emit();
        """)
    # x = [0.1, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0]
    # y0 = [i**2 for i in x]
    # y1 = [10**i for i in x]
    # y2 = [10**(i**2) for i in x]

    # sline = ColumnDataSource(data=dict(x=x, y=x))
    # sy0 = ColumnDataSource(data=dict(x=x, y=y0))
    # sy1 = ColumnDataSource(data=dict(x=x, y=y1))
    # sy2 = ColumnDataSource(data=dict(x=x, y=y2))
    # # output to static HTML file
    # # output_file("html/log_lines.html")

    # # create a new plot
    # p = figure(
       # tools="pan,lasso_select,box_select,box_zoom,reset,save",
       # y_axis_type="log", y_range=[0.001, 10**11], title="log axis example",
       # x_axis_label='sections', y_axis_label='particles'
    # )

    # # add some renderers
    # # p.line('x', 'x', source=sline, legend="y=x")
    # # p.circle('x', 'x',source=sline, legend="y=x", fill_color="white", size=8)
    # # p.line('x', 'y',source=sy0, legend="y=x^2", line_width=3)
    # # p.circle('x', 'y',source=sy0, legend="y=x^2", line_width=3)
    # # p.line('x', 'y',source=sy1, legend="y=10^x", line_color="red")
    # # p.circle('x', 'y', source=sy1,legend="y=10^x", fill_color="red", line_color="red", size=6)
    # p.line('x', 'y', source=sy2,legend="y=10^x^2", line_color="orange")
    # p.circle('x', 'y', source=sy2,legend="y=10^x^2", line_color="orange", line_dash="4 4")

    # sy2.callback = CustomJS(args=dict(sy2=sy2), code="""
        # var inds = cb_obj.selected.indices; var d1 = cb_obj.data;

        # console.log(inds);
        # console.log(d1)
    # """)
    # p.background_fill_alpha = 0
    # p.border_fill_alpha = 0

    # # res = file_html(p, INLINE, "My plot")
    layout= column(row(p1,table),row(button))

    script, div = components(layout, wrap_script=False)

    res = {
        "script": script,
        "div": div,
    }
    return flask.jsonify(res = res)

app.run(debug=True)
