var data = source.data;
var filetext = 'id,x,y\n';
console.log(data['id'])
for (i=0; i < data['id'].length; i++) {
    var currRow = [data['id'][i].toString(),
                   data['x'][i].toString(),
                   data['y'][i].toString().concat('\n')];

    var joined = currRow.join();
    console.log(joined)
    filetext = filetext.concat(joined);
}

var filename = 'data_result.csv';
var blob = new Blob([filetext], { type: 'text/csv;charset=utf-8;' });

console.log(filetext)

//addresses IE
if (navigator.msSaveBlob) {
    navigator.msSaveBlob(blob, filename);
}

else {
    var link = document.createElement("a");
    link = document.createElement('a')
    link.href = URL.createObjectURL(blob);
    link.download = filename
    link.target = "_blank";
    link.style.visibility = 'hidden';
    link.dispatchEvent(new MouseEvent('click'))
}
