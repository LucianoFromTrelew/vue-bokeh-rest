import DashboardLayout from '../components/Dashboard/Layout/DashboardLayout.vue'
import Typography from '../components/Dashboard/Views/Typography.vue'
// GeneralViews
import NotFound from '../components/GeneralViews/NotFoundPage.vue'

// Admin pages
import Overview from 'src/components/Dashboard/Views/Overview.vue'

const routes = [
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/admin/overview'
  },
  {
    path: '/admin',
    component: DashboardLayout,
    redirect: '/admin/stats',
    children: [
      {
        path: 'overview',
        name: 'overview',
        component: Overview
      },
      {
        path: 'bokeh',
        name: 'bokeh',
        component: Typography
      }
    ]
  },
  { path: '*', component: NotFound }
]

export default routes
